from django.http import HttpResponseRedirect
from django.contrib import auth
from django.views.generic.edit import FormView
from Final_project.forms import ProfilCreationForm
from django.contrib.auth import login
from django.contrib.auth.forms import AuthenticationForm
from django.views import generic
from .models import Goods



class RegisterFormView(FormView):
    form_class = ProfilCreationForm
    success_url = "/"
    template_name = "registration.html"


    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self,).form_valid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "login.html"
    success_url = "/"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect("http://127.0.0.1:8000/")


class GoodsView(generic.ListView):
    template_name = 'main.html'
    context_object_name = 'case_list'

    def get_queryset(self):
        goods = Goods.objects.all ()
        return goods

