from django.db import models
from django.contrib.auth.models import User


class Goods(models.Model):
    ProductID = models.AutoField(primary_key=True, null=False, unique=True)
    ProductName = models.CharField(max_length=256)
    ProductPrice = models.IntegerField()
    Category = models.CharField(max_length=50)
    ProductDescription = models.TextField()
    Calories = models.DecimalField(max_digits=10, decimal_places=2)
    ImagePath = models.ImageField(blank=True)

    def __str__(self):
        return '{0.ProductName}'.format(self)
